# Suggester

[![Build Status](https://drone.gitea.com/api/badges/lunny/suggester/status.svg)](https://drone.gitea.com/lunny/suggester)  [![](http://gocover.io/_badge/gitea.com/lunny/suggester)](http://gocover.io/gitea.com/lunny/suggester)
[![](https://goreportcard.com/badge/gitea.com/lunny/suggester)](https://goreportcard.com/report/gitea.com/lunny/suggester)

Suggester is designed as a web service for providing input suggestions. It has 4 URLs,

* Status

`GET /:prefix/status`

* Add Index

`PUT /:prefix/:unit_id/:word/:id`

* Del Index

`DELETE /:prefix/:unit_id/:word/:id`

* Search Index

`GET /:prefix/:unit_id/:word`

Also you can use `suggester` sub package to operate it.