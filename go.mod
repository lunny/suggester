module gitea.com/lunny/suggester

go 1.12

require (
	gitea.com/lunny/log v0.0.0-20190322053110-01b5df579c4e
	gitea.com/lunny/tango v0.6.0
	gitea.com/tango/debug v0.0.0-20190606021531-83f604669a11
	github.com/stretchr/testify v1.3.0
	github.com/syndtr/goleveldb v1.0.0
)
